const express = require("express");

// Create an application using express
const app = express();

const port = 3000;

// Allows your app to read JSON data
app.use(express.json());

// Allows your app to read data in any forms
app.use(express.urlencoded({extended:true}));

let users = [{
	username: "johndoe",
	password: "johndoe1234",
},
{
	username: "janesmith",
	password: "janesmith1234"
}]

// GET
// This route expects to receive a "GET" at the "/greet" endpoint
app.get("/home", (request, response) => {
	// "respond.send" uses the express.js module's method to send a response back to the client
	response.send("Hello! Welcome to the homepage")
});

app.get("/users", (request, response) => {
	// "respond.send" uses the express.js module's method to send a response back to the client
	response.send(users)
});

app.delete("/delete-user", (request, response) => {
	// "respond.send" uses the express.js module's method to send a response back to the client
	// response.send(`User ${request.body.username} has been deleted`)
	let message;

	// Creates a for loop that will loop through the elements of the "users" array 
	for(let i = 0; i < users.length; i++){

		// If the username provided in the client/Postman and the username of the current object in the loop is the same
		if(request.body.username == users[i].username && request.body.password == users[i].password){

			message = `User ${request.body.username}'s password has been deleted.`
			break;
		} else {
			message = "User does not exist"
		}
	}
	response.send(message);
});

app.listen(port, ()=> console.log(`Server running at port ${port}`));